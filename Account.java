class Account 
{	
  private int pin;      	  	// 4 digit pin number
  private String name;	        // First and last name of the bank account owner
  private double[] balance = new double[2];  
  private int accountNum;       // Unique account number for the bank account. used as the key for searching and sorting
  private static int numOfAccounts = 0;    // used to generate the unique act. Nums
  private static String adminPassword = “hci2022”;

  public Account (String name, int pin) 
  {
    this.pin = pin;
    this.name = name;

    numOfAccounts++;

    this.accountNum = numOfAccounts;
  }

  public void withdraw (int accountNum, int pin, double amount, int whichAccount)
  {
    if (this.accountNum != accountNum || this.pin != pin) throw new RuntimeException("Incorrect login.");
    else if (amount > this.balance[whichAccount]) throw new RuntimeException("Cannot withdraw more money than is in your account.");
    else {
        this.balance[whichAccount] -= amount;
    }
  }

  public void deposit (int accountNum, int pin, double amount, int whichAccount)
  {
    if (this.accountNum != accountNum || this.pin != pin) throw new RuntimeException("Incorrect login.");
    else {
        this.balance[whichAccount] += amount;
    }
  }


}